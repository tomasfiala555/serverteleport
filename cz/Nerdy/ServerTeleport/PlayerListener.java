package cz.Nerdy.ServerTeleport;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;


public class PlayerListener implements Listener {

    @EventHandler
    public void onManipulate(PlayerArmorStandManipulateEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onInteract(PlayerInteractAtEntityEvent e) {
        Player p = e.getPlayer();
        if (e.getRightClicked() instanceof ArmorStand) {
            if(e.getRightClicked().hasMetadata("creative")){
                Main.getInstance().teleportToServer(p, "creative");
            }
            if(e.getRightClicked().hasMetadata("survival")){
                Main.getInstance().teleportToServer(p, "survival");
            }
            if(e.getRightClicked().hasMetadata("pvp")){
                Main.getInstance().teleportToServer(p, "pvp");
            }
            if(e.getRightClicked().hasMetadata("minigames")){
                Main.getInstance().teleportToServer(p, "minigames");
            }
            e.setCancelled(true);
        }
    }
}
