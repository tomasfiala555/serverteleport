package cz.Nerdy.ServerTeleport;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;

public class PvP implements Listener {
    private static Main plugin;
    ArmorStand as, as2, as3, as4, as5;
    Location loc = new Location(Bukkit.getWorld("Lobby"), -240.492, 70.0, 270.542, -42.4F, 0.8F);


    public static void open(Player p) {
        Inventory inv = Bukkit.createInventory(null, 36, "§c§lPvP");
        /*ItemStack item = new ItemStack(Material.BEDROCK, getPlayers());
        ItemMeta meta = Bukkit.getItemFactory().getItemMeta(Material.BEDROCK);
        meta.setDisplayName("§a§lCreative");
        ArrayList<String> lore = new ArrayList<String>();
        lore.add("");
        //lore.add("§7Hráči: " + ChatColor.GREEN + getPlayers() + "/50");
        lore.add("");
        lore.add("§7§lPřipoj se!");

        meta.setLore(lore);
        item.setItemMeta(meta);

        inv.setItem(13, item);*/

        ItemStack creative = new ItemStack(Material.REDSTONE_BLOCK);
        ItemMeta creativeMeta = Bukkit.getItemFactory().getItemMeta(Material.REDSTONE_BLOCK);
        creativeMeta.setDisplayName("§c§lPvP");
        ArrayList<String> creativeLore = new ArrayList<String>();
        creativeLore.add(" ");
        creativeLore.add("§7Hráči: " + ChatColor.RED + getPlayers() + "/50");
        creativeLore.add("");
        creativeLore.add("§7§lPřipoj se!");
        creativeMeta.setLore(creativeLore);
        creative.setItemMeta(creativeMeta);
        inv.setItem(13, creative);
        p.openInventory(inv);
    }

    public void spawn() {
        as = (ArmorStand) Bukkit.getWorld("Lobby").spawnEntity(loc, EntityType.ARMOR_STAND);
        as.setGravity(false);
        as.setVisible(true);
        as.setCanPickupItems(false);
        as.setBasePlate(false);
        as.setArms(true);
        as.setCustomNameVisible(false);
        as.setHelmet(Main.getInstance().createHead("pvp", "125c5512-a63b-4e6a-a710-e7afd3a4aa9a", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZWI5OTRiNDFmMDdmODdiMzI4MTg2YWNmY2JkYWJjNjk5ZDViMTg0N2ZhYmIyZTQ5ZDVhYmMyNzg2NTE0M2E0ZSJ9fX0="));
        as.setChestplate(Main.getInstance().createColouredLeather(Material.LEATHER_CHESTPLATE, 193, 51, 3));
        as.setLeggings(Main.getInstance().createColouredLeather(Material.LEATHER_LEGGINGS, 193, 51, 3));
        as.setBoots(Main.getInstance().createColouredLeather(Material.LEATHER_BOOTS, 193, 51, 3));
        Main.getInstance().setMetadata((ArmorStand) as, "pvp", "pvp", Main.getInstance());

        spawnLine();
        spawnHolo();
        spawnName();
        spawnLine2();

        Bukkit.getLogger().log(Level.INFO, "§ePvP §bArmorStand §euspesne spawnut!");
    }

    public void spawnName() {
        loc.add(0, 0.3, 0);

        as2 = (ArmorStand) Bukkit.getWorld("Lobby").spawnEntity(loc, EntityType.ARMOR_STAND);
        as2.setGravity(false);
        as2.setCanPickupItems(false);
        as2.setBasePlate(false);
        as2.setVisible(false);
        as2.setCustomNameVisible(true);
        as2.setCustomName("§c§lPVP");
    }

    public void spawnHolo() {
        loc.add(0, 0.3, 0);
        as3 = (ArmorStand) Bukkit.getWorld("Lobby").spawnEntity(loc, EntityType.ARMOR_STAND);
        as3.setGravity(false);
        as3.setCanPickupItems(false);
        as3.setBasePlate(false);
        as3.setVisible(false);
        as3.setCustomNameVisible(true);
        as3.setCustomName("§fPocet hracu: §c0");
    }

    public void spawnLine() {
        //loc.add(0, 0.3, 0);
        as4 = (ArmorStand) Bukkit.getWorld("Lobby").spawnEntity(loc, EntityType.ARMOR_STAND);
        as4.setGravity(false);
        as4.setCanPickupItems(false);
        as4.setBasePlate(false);
        as4.setVisible(false);
        as4.setCustomNameVisible(true);
        as4.setCustomName("§c§f§m--------§c /\\ §f§m--------§c");
    }

    public void spawnLine2() {
        loc.add(0, 0.3, 0);
        as5 = (ArmorStand) Bukkit.getWorld("Lobby").spawnEntity(loc, EntityType.ARMOR_STAND);
        as5.setGravity(false);
        as5.setCanPickupItems(false);
        as5.setBasePlate(false);
        as5.setVisible(false);
        as5.setCustomNameVisible(true);
        as5.setCustomName("§c§f§m--------§c \\/ §f§m--------§c");
    }

    public void remove() {
        as.remove();
        as2.remove();
        as3.remove();
        as4.remove();
        as5.remove();
    }

    public void updatePlayers() {
        as3.setCustomName("§fPocet hracu: §c" + getPlayers());
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (e.getClickedInventory() == null) {
            return;
        }
        if (e.getClickedInventory().getName() == null) {
            return;
        }
        if (!e.getClickedInventory().getName().equalsIgnoreCase("§c§lPvP")) {
            return;
        }
        e.setCancelled(true);
        if (!e.getCurrentItem().hasItemMeta()) {
            return;
        }
        if (!e.getCurrentItem().getItemMeta().hasDisplayName()) {
            return;
        }
        Player p = (Player) e.getWhoClicked();
        if (e.getCurrentItem().getType() == Material.REDSTONE_BLOCK) {
            Main.getInstance().teleportToServer(p, "pvp");
        }
        p.closeInventory();
    }

    public static int getPlayers() {
        try {
            Socket sock = new Socket("89.203.249.235", 25569);

            DataOutputStream out = new DataOutputStream(sock.getOutputStream());
            DataInputStream in = new DataInputStream(sock.getInputStream());

            out.write(0xFE);

            int b;
            StringBuffer str = new StringBuffer();
            while ((b = in.read()) != -1) {
                if (b != 0 && b > 16 && b != 255 && b != 23 && b != 24) {
                    str.append((char) b);
                }
            }

            String[] data = str.toString().split("§");
            int onlinePlayers = Integer.parseInt(data[1]);
            return onlinePlayers;

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }


}
